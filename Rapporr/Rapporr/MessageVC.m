//
//  MessageVC.m
//  Rapporr
//
//  Created by Ahmed Sadiq on 06/04/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "MessageVC.h"
#import "MessageCell.h"
#import "NetworkManager.h"
#import "DataManager.h"
#import "Constants.h"
#import "RapporrManager.h"
#import "VCFloatingActionButton.h"
#import "CoreDataController.h"
#import "ConversationUser.h"
#import "UIImageView+Letters.h"
#import <QuartzCore/QuartzCore.h>
#import "RKDropdownAlert.h"
#import "CreateConversationVC.h"



@interface MessageVC ()

@end

@implementation MessageVC
@synthesize addMenuButton;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    self.internetReachability = delegate.internetReachability;
    [self.internetReachability startNotifier];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receivedNotificationWithVerifiedCompanyModel:)
                                                 name:@"VerifiedCompanyModel" object:nil];
    
    _refresh = [[UIRefreshControl alloc] init];
    _refresh.backgroundColor = [UIColor clearColor];
    [_refresh addTarget:self action:@selector(getUserList) forControlEvents:UIControlEventValueChanged];
    [self.mainTblView addSubview:self.refresh];
    
    
    [self adjustTabBarImageOffset];
    [self addFloatingButton];
    [self addScrollToTop];
   
    [self fetchMessagesFromDB];
    [self fetchUserFromDB];
    
    [self getUserList];
    [self setUpAlertView];
    
}

- (void)stopRefresh
{
    [self.refresh endRefreshing];
}

- (void)receivedNotificationWithVerifiedCompanyModel:(NSNotification *) notification {
    
    NSDictionary *userInfo = notification.object;
    VerifiedCompanyModel *company = [userInfo objectForKey:@"CompanyModel"];
    self.vcModel = company;
    self.lblTitle.text = self.vcModel.companyName;
}


- (void) setUpAlertView {
    _customAlert = [[RapporrAlertView alloc] init];
    [_customAlert setDelegate:self];
}

-(void)updateData{

    headersArray = [Utils getTableViewSectionByDates];
    [self fetchMessagesFromDB];
    [self fetchUserFromDB];
    [self sortMessagesDictionary];
    [_mainTblView reloadData];
    [self sortMessagesDictionaryForSearchedItems];
    [self performSelector:@selector(stopRefresh) withObject:nil afterDelay:2.5];

    [searchTblView reloadData];
    
    
    /*if(!isSearching) {
        [self sortMessagesDictionary];
        [_mainTblView reloadData];
    }
    else {
        [self sortMessagesDictionaryForSearchedItems];
        [searchTblView reloadData];
    }*/
}




-(void)viewWillAppear:(BOOL)animated{}

/*!
 * Called by Reachability whenever status changes.
 */
//- (void) reachabilityChanged:(NSNotification *)note
//{
//    Reachability* curReach = [note object];
//    NSParameterAssert([curReach isKindOfClass:[Reachability class]]);
//    [self updateInterfaceWithReachability:curReach];
//}

- (void)updateInterfaceWithReachability:(Reachability *)reachability
{
    if (reachability == self.internetReachability)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            NetworkStatus netStatus = [reachability currentReachabilityStatus];
            BOOL connectionRequired = [reachability connectionRequired];
            NSString* statusString = @"";
            
            if (netStatus == NotReachable)
            {
                statusString = NSLocalizedString(@"No Internet Connection", @"access is not available");
                
                [RKDropdownAlert dismissAllAlert];
                [RKDropdownAlert title:@"" message:@"No Internet Connection" backgroundColor:[UIColor colorWithCSS:@"#FF5721"] textColor:[UIColor whiteColor] time:5 delegate:nil autoHide:NO];
                connectionRequired = NO;
                
            }else if (netStatus == ReachableViaWWAN || netStatus == ReachableViaWiFi){
                [RKDropdownAlert dismissAllAlert];
                //                [self updateData];
                statusString = NSLocalizedString(@"Connected", @"");
                [RKDropdownAlert title:@"" message:@"Connected" backgroundColor:[UIColor colorWithCSS:@"#00B36D"] textColor:[UIColor whiteColor] time:5 delegate:nil autoHide:YES];
                connectionRequired = YES;
            }
        });
    }
}

- (void) fetchUserFromDB {
    
    NSManagedObjectContext *managedObjectContext = [delegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"ConverstionUser"];
    NSMutableArray *tempUsers = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    users = [[NSMutableArray alloc] init];
    
    for(int i=0; i<tempUsers.count; i++) {
        NSManagedObject *conversationUserObj = [tempUsers objectAtIndex:i];
        ConversationUser *cUserModel = [[ConversationUser alloc] initWithManagedObject:conversationUserObj];
        [users addObject:cUserModel];
    }
    
    if(users.count >0) {
        ConversationUser *conversationUserObj = [users objectAtIndex:0];
        
        userTimeStamp = conversationUserObj.timeStamp;
    }
    else {
        userTimeStamp = @"all";
    }
}
- (void) fetchMessagesFromDB {
    
    NSManagedObjectContext *managedObjectContext = [delegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Message"];
    NSMutableArray *tempMessages = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    messages = [[NSMutableArray alloc] init];
    
    for(int i=0; i<tempMessages.count; i++) {
        NSManagedObject *messageObj = [tempMessages objectAtIndex:i];
        MessageModel *mModel = [[MessageModel alloc] initWithManagedObject:messageObj];
        [messages addObject:mModel];
    }
    
    if(messages.count >0) {
        MessageModel *messageObj = [messages objectAtIndex:0];
        timeStamp = messageObj.timeStamp;
    }
    else {
        timeStamp = @"all";
    }
}

- (void) getUserList {
    NSDictionary *paramsToBeSent = [NSDictionary dictionaryWithObjectsAndKeys:[RapporrManager sharedManager].vcModel.hostID,@"hostId",[RapporrManager sharedManager].vcModel.userId,@"userID",nil];
    
    NSString *uri = [NSString stringWithFormat:@"%@%@/users?last=%@",URI_GET_USERS,[RapporrManager sharedManager].vcModel.orgId,userTimeStamp];
    
    [NetworkManager fetchConversationAPI:uri parameters:paramsToBeSent success:^(id data) {
        
        NSError* error;
        NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData: data options: NSJSONReadingMutableContainers error: &error];
        
        NSLog(@"%@",jsonArray);
        
        for(int i=0; i<jsonArray.count; i++) {
            ConversationUser *conversationUserModel = [[ConversationUser alloc] initWithDictionary:(NSDictionary*)[jsonArray objectAtIndex:i]];
            [users addObject:conversationUserModel];
            
            [[CoreDataController sharedManager] saveConversationUser:conversationUserModel];
        }
        
        [self getConversationList];
        
    }failure:^(NSError *error) {
        NSLog(@"Error");
    }];
}

- (void) getConversationList {
    NSDictionary *paramsToBeSent = [NSDictionary dictionaryWithObjectsAndKeys:[RapporrManager sharedManager].vcModel.hostID,@"hostId",[RapporrManager sharedManager].vcModel.userId,@"userID",nil];
    
    NSString *uri = [NSString stringWithFormat:@"%@%@?last=%@",URI_GET_CONVERSATION,[RapporrManager sharedManager].vcModel.userId,timeStamp];
    [NetworkManager fetchConversationAPI:uri parameters:paramsToBeSent success:^(id data) {
        
        
        NSError* error;
        NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData: data options: NSJSONReadingMutableContainers error: &error];
        
        
        for(int i=0; i<jsonArray.count; i++) {
            MessageModel *mModel = [[MessageModel alloc] initWithDictionary:(NSDictionary*)[jsonArray objectAtIndex:i]];
            [self saveMessageModel:mModel];
        }
        [self updateData];
    
        
    }failure:^(NSError *error) {
        NSLog(@"Error");
    }];
}

- (void) saveMessageModel : (MessageModel*) mModel {
    
    [[CoreDataController sharedManager] saveMessageModel:mModel];
}

- (void) adjustTabBarImageOffset {
    UITabBar *tabBar = self.navigationController.tabBarController.tabBar;
    UITabBarItem *tabBarItem1 = [tabBar.items objectAtIndex:0];
    
    tabBarItem1.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
    self.title = nil;
}

- (void)addFloatingButton {
    CGRect floatFrame = CGRectMake([UIScreen mainScreen].bounds.size.width - 44 - 20, [UIScreen mainScreen].bounds.size.height - 44 - 60, 44, 44);
    
    addMenuButton = [[VCFloatingActionButton alloc]initWithFrame:floatFrame normalImage:[UIImage imageNamed:@"edit"] andPressedImage:[UIImage imageNamed:@"edit"] withScrollview:_mainTblView];
    
    addMenuButton.imageArray = @[@"fb-icon",@"twitter-icon",@"google-icon",@"linkedin-icon"];
    addMenuButton.labelArray = @[@"Facebook",@"Twitter",@"Google Plus",@"Linked in"];
    
    addMenuButton.hideWhileScrolling = YES;
    addMenuButton.delegate = self;
    
    [self.view addSubview:addMenuButton];
}

- (void)addScrollToTop {
    
    MEVFloatingButton *button = [[MEVFloatingButton alloc] init];
    button.animationType = MEVFloatingButtonAnimationFromBottom;
    button.displayMode = MEVFloatingButtonDisplayModeWhenScrolling;
    button.position = MEVFloatingButtonPositionTopCenter;
    [button setImage:[UIImage imageNamed:@"btnMoveTop"]];
    button.backgroundColor = [UIColor colorWithCSS:@"#FF5721"];
    button.outlineWidth = 0.0f;
    button.imagePadding = 0.0f;
    button.horizontalOffset = 0.0f;
    button.verticalOffset = 0.0f;
    button.rounded = YES;
    button.title = NSLocalizedString(@"TOP", nil);
    button.hideWhenScrollToTop = YES;
    [self.mainTblView setFloatingButtonView:button];
    [self.mainTblView setFloatingButtonDelegate:self];
    
}

#pragma mark - MEScrollToTopDelegate Methods

- (void)floatingButton:(UIScrollView *)scrollView didTapButton:(UIButton *)button {
    
    NSLog(@"didTapButton");
    [self.mainTblView setContentOffset:CGPointMake(0, -self.mainTblView.contentInset.top) animated:YES];
}

- (void)floatingButtonWillAppear:(UIScrollView *)scrollView {
    NSLog(@"floatingButtonWillAppear");
}

- (void)floatingButtonDidAppear:(UIScrollView *)scrollView {
    NSLog(@"floatingButtonDidAppear");
}

- (void)floatingButtonWillDisappear:(UIScrollView *)scrollView {
    NSLog(@"floatingButtonWillDisappear");
}

- (void)floatingButtonDidDisappear:(UIScrollView *)scrollView; {
    NSLog(@"floatingButtonDidDisappear");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark -
#pragma mark Table View Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        searchTblView = self.searchDisplayController.searchResultsTableView;
        return messagesDictArrayforSearch.count;
        
    } else {
        
        return messagesDictArray.count;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        NSMutableDictionary *tempDict = (NSMutableDictionary*)[messagesDictArrayforSearch objectAtIndex:section];
        return [tempDict objectForKey:@"day"];
        
    } else {
        
        NSMutableDictionary *tempDict = (NSMutableDictionary*)[messagesDictArray objectAtIndex:section];
        return [tempDict objectForKey:@"day"];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    view.tintColor = App_GrayColor;
    view.opaque = NO;
    self.mainTblView.separatorColor = [UIColor clearColor];
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setTextColor:[UIColor blackColor]];
    [header.textLabel setFont:[UIFont fontWithName:@"Roboto-Regular" size:17]];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        NSMutableDictionary *tempDict = (NSMutableDictionary*)[messagesDictArrayforSearch objectAtIndex:section];
        NSMutableArray *arrayOfSection = [tempDict objectForKey:@"array"];
        return arrayOfSection.count;
        
    } else {
        
        NSMutableDictionary *tempDict = (NSMutableDictionary*)[messagesDictArray objectAtIndex:section];
        NSMutableArray *arrayOfSection = [tempDict objectForKey:@"array"];
        return arrayOfSection.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    MessageCell * cell = (MessageCell *)[tableView dequeueReusableCellWithIdentifier:@"MessageCell"];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"MessageCell" owner:self options:nil];
        cell=[nib objectAtIndex:0];
    }
    
    cell.imgView.layer.cornerRadius = 5;
    cell.imgView.clipsToBounds = YES;
    MessageModel *mModel;
    
    cell.layoutMargins = UIEdgeInsetsZero;

    
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];

        tableView.frame = _mainTblView.frame;
        isSearching = true;
        NSMutableDictionary *tempDict = (NSMutableDictionary*)[messagesDictArrayforSearch objectAtIndex:indexPath.section];

        NSMutableArray *arrayOfSection = [tempDict objectForKey:@"array"];
        mModel = (MessageModel*)[arrayOfSection objectAtIndex:indexPath.row];
        
    } else {
        isSearching = false;
        NSMutableDictionary *tempDict = (NSMutableDictionary*)[messagesDictArray objectAtIndex:indexPath.section];
        NSMutableArray *arrayOfSection = [tempDict objectForKey:@"array"];
        mModel = (MessageModel*)[arrayOfSection objectAtIndex:indexPath.row];
    }
    
    NSMutableArray *usersInMessage = [self usersForMessage:mModel];
    
    ConversationUser *cUser = [usersInMessage objectAtIndex:usersInMessage.count-1];
    
    NSLog(@"%@",cUser.avatarUrl);

    if(cUser.avatarUrl.length > 1) {
        cell.imgView.imageURL = [NSURL URLWithString: cUser.avatarUrl];
        NSURL *url = [NSURL URLWithString:cUser.avatarUrl];
        [[AsyncImageLoader sharedLoader] loadImageWithURL:url];
    }
    else {
        [cell.imgView setImageWithString:cUser.name color:[UIColor colorWithCSS:@"#FF9433"] circular:NO];
    }
    
    cell.tag = indexPath.section;
    cell.accessibilityHint = [NSString stringWithFormat:@"%ld",(long)indexPath.row];
    cell.delegate = self;
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        cell.lblNumberUsers.text = [NSString stringWithFormat:@"%ld",[usersInMessage count]];
        cell.titleLbl.text = [mModel.about uppercaseString];
        cell.subTitle1Lbl.text = [self getSubTitleLbl:usersInMessage];
        cell.subTitle2Lbl.text = [self messageWithSender:mModel];
        cell.dateLbl.text = [self dateTimeForConversation:mModel];
        self.mainTblView.separatorColor = [UIColor clearColor];

        
        if (mModel.isPinned) {
            [cell.imgViewPinConv setImage:[UIImage imageNamed:@"pinSelectedIcon"]];
        }else{
            
        }
        [cell setRightUtilityButtons:[self leftButtonsWithMessageModel:mModel] WithButtonWidth:70.0f];
    });
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //    MessageModel *mModel;
    //
    //    if (tableView == self.searchDisplayController.searchResultsTableView) {
    //        tableView.frame = _mainTblView.frame;
    //        isSearching = true;
    //        NSMutableDictionary *tempDict = (NSMutableDictionary*)[messagesDictArrayforSearch objectAtIndex:indexPath.section];
    //        NSMutableArray *arrayOfSection = [tempDict objectForKey:@"array"];
    //        mModel = (MessageModel*)[arrayOfSection objectAtIndex:indexPath.row];
    //
    //    } else {
    //        isSearching = false;
    //        NSMutableDictionary *tempDict = (NSMutableDictionary*)[messagesDictArray objectAtIndex:indexPath.section];
    //        NSMutableArray *arrayOfSection = [tempDict objectForKey:@"array"];
    //        mModel = (MessageModel*)[arrayOfSection objectAtIndex:indexPath.row];
    //    }
    //
    //    NSString *message = [NSString stringWithFormat:@"%@: %@",mModel.senderName,mModel.lastMsg];
    //
    //    NSStringDrawingContext *ctx = [NSStringDrawingContext new];
    //    NSAttributedString *aString = [[NSAttributedString alloc] initWithString:message];
    //    UITextView *calculationView = [[UITextView alloc] init];
    //    [calculationView setAttributedText:aString];
    //    CGRect textRect = [calculationView.text boundingRectWithSize:self.view.frame.size options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:calculationView.font} context:ctx];
    
    //    return textRect.size.height + 100;
    return 98;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ConversationDetailVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ConversationDetailVC"];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (NSString*) getTitleLbl : (NSMutableArray*)usersInMessage {
    NSString *titlelbl = @"";
    if(usersInMessage.count>0){
        ConversationUser *cUser = [usersInMessage objectAtIndex:0];
        titlelbl = cUser.name;
    }
    
    for(int i=1; i<usersInMessage.count; i++){
        ConversationUser *cUser = [usersInMessage objectAtIndex:i];
        titlelbl = [NSString stringWithFormat:@"%@ and %@",titlelbl,cUser.name];
    }
    return titlelbl;
}

- (NSString*) getSubTitleLbl : (NSMutableArray*)usersInMessage {
    NSString *titlelbl = @"";
    if(usersInMessage.count>0){
        ConversationUser *cUser = [usersInMessage objectAtIndex:0];
        titlelbl = cUser.name;
    }
    
    for(int i=1; i<usersInMessage.count -1; i++){
        ConversationUser *cUser = [usersInMessage objectAtIndex:i];
        titlelbl = [NSString stringWithFormat:@"%@, %@",titlelbl,cUser.name];
    }
    ConversationUser *cUser = [usersInMessage lastObject];
    titlelbl = [NSString stringWithFormat:@"%@ and %@",titlelbl,cUser.name];
    
    return titlelbl;
}

- (NSString*)messageWithSender:(MessageModel*)message {
    NSString *titlelbl = [NSString stringWithFormat:@"%@: %@",message.senderName,message.lastMsg];
    return titlelbl;
}

- (NSString *)dateTimeForConversation:(MessageModel*)message{
    
    NSString *dateString = message.timeStamp;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSLocale *enUSPosixLocale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:enUSPosixLocale];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ"];
    NSDate *dateFromMessage = [formatter dateFromString:dateString];
    NSString *dateTimeString = @"";
   
    NSDate *currentDate = [NSDate getDateInCurrentSystemTimeZone];
    
    
    NSLog(@"%ld",(long)[NSDate daysBetweenDate:currentDate andDate:dateFromMessage]);
    
    NSInteger isSameDay = [NSDate daysBetweenDate:currentDate andDate:dateFromMessage];
    if(isSameDay >= 0) {
        [formatter setDateFormat:@"HH:mm a"];
        dateTimeString = [formatter stringFromDate:dateFromMessage];
    }else{
        [formatter setDateFormat:@"MMM d"];
        dateTimeString = [formatter stringFromDate:dateFromMessage];
    }
    return dateTimeString;
}


- (NSMutableArray *)usersForMessage :(MessageModel*)mModel {
    NSMutableArray *usersArray = [[NSMutableArray alloc] init];
    
    for(int i=0; i<mModel.users.count; i++){
        NSString *userId = [mModel.users objectAtIndex:i];
        ConversationUser *uUser = [self getUserfromUsersList:userId];
        if(uUser)
            [usersArray addObject:uUser];
    }
    
    return usersArray;
}

- (ConversationUser*) getUserfromUsersList :(NSString*)userID {
    for (int i=0; i<users.count; i++) {
        ConversationUser *cUser = [users objectAtIndex:i];
        if([cUser.fullId isEqualToString:userID]){
            return cUser;
        }
    }
    return nil;
}

- (NSArray *)leftButtonsWithMessageModel:(MessageModel*)msgModel
{
    
    NSMutableArray *leftUtilityButtons = [NSMutableArray new];
    
    if(msgModel.isPinned){
        [leftUtilityButtons sw_addUtilityButtonWithColor:
         [UIColor colorWithRed:0.113 green:0.443f blue:0.639f alpha:1.0] icon:
         [UIImage imageNamed:@"unpin"] andTitle:@"Unpin"];
        
    }else{
        [leftUtilityButtons sw_addUtilityButtonWithColor:
         [UIColor colorWithRed:0.113 green:0.443f blue:0.639f alpha:1.0] icon:
         [UIImage imageNamed:@"pin"] andTitle:@"Pin"];
    }
    
    
    [leftUtilityButtons sw_addUtilityButtonWithColor: [UIColor colorWithRed:0.113f green:0.443f blue:0.639f alpha:1.0] icon:[UIImage imageNamed:@"archive"] andTitle:@"Archive"];
    
    return leftUtilityButtons;
}




- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF.name contains[cd] %@", searchText];
    
    //    searchResults = [messages filteredArrayUsingPredicate:resultPredicate];
    
    searchResults = [[NSMutableArray alloc]init];
    
    //    Title Search
    NSArray *messagesArrayWithTitle = [messages filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.about contains[cd] %@", searchText]];
    
    if ([messagesArrayWithTitle count]) {
        searchResults = [NSMutableArray arrayWithArray:messagesArrayWithTitle];
    }else{
        
        //    User Search
        for (MessageModel *mModel in messages) {
            NSMutableArray *usersInMessage = [self usersForMessage:mModel];
            NSArray *conversationUsers = [usersInMessage filteredArrayUsingPredicate:resultPredicate];
            
            for (ConversationUser *user in usersInMessage) {
                BOOL isSearched = false;
                for (ConversationUser *convUser in conversationUsers) {
                    NSLog(@"user.fullId %@ :>  convUser.fullId %@",user.fullId,convUser.fullId);
                    if ([user.fullId isEqualToString:convUser.fullId]) {
                        isSearched = true;
                        [searchResults addObject:mModel];
                        break;
                    }
                }
                if(isSearched) {
                    break;
                }
            }
        }
    }
    headersArray = [Utils getTableViewSectionByDates];
    [self sortMessagesDictionaryForSearchedItems];
}


-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}

-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    
    self.mainTblView.frame = CGRectMake(0,80,self.mainTblView.frame.size.width, self.mainTblView.frame.size.height);
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if([searchText isEqualToString:@""] || searchText==nil) {
        isSearching = false;
        [self updateData];
    }
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    
    self.mainTblView.frame = CGRectMake(0,80,self.mainTblView.frame.size.width, self.mainTblView.frame.size.height);
    
    return YES;
}

-(void) didSelectMenuOptionAtIndex:(NSInteger)row
{
    NSLog(@"Floating action tapped index %tu",row);
}

-(void) didMenuButtonTapped;
{
    CreateConversationVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"CreateConversationVC"];
    [self.navigationController pushViewController:vc animated:NO];
}

-(void) didScrollToTopButtonTapped;
{
    [_mainTblView setScrollsToTop:YES];
}


#pragma mark - SWTableViewDelegate

- (void)swipeableTableViewCell:(SWTableViewCell *)cell scrollingToState:(SWCellState)state
{
    switch (state) {
        case 0:
            NSLog(@"utility buttons closed");
            break;
        case 1:
            NSLog(@"left utility buttons open");
            break;
        case 2:
            NSLog(@"right utility buttons open");
            break;
        default:
            break;
    }
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index
{
    switch (index) {
        case 0:
            NSLog(@"left button 0 was pressed");
            break;
        case 1:
            NSLog(@"left button 1 was pressed");
            break;
        case 2:
            NSLog(@"left button 2 was pressed");
            break;
        case 3:
            NSLog(@"left btton 3 was pressed");
        default:
            break;
    }
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    MessageModel *mModel;
    if(isSearching) {
        
        NSMutableDictionary *tempDict = nil;
        
        if ([messagesDictArrayforSearch count]) {
            tempDict = (NSMutableDictionary*)[messagesDictArrayforSearch objectAtIndex:cell.tag];
        }
        NSMutableArray *arrayOfSection = [tempDict objectForKey:@"array"];
        int rowIndex = [cell.accessibilityHint intValue];
        if ([arrayOfSection count]) {
            mModel = (MessageModel*)[arrayOfSection objectAtIndex:rowIndex];
        }
    }
    else {
        NSMutableDictionary *tempDict = (NSMutableDictionary*)[messagesDictArray objectAtIndex:cell.tag];
        NSMutableArray *arrayOfSection = [tempDict objectForKey:@"array"];
        int rowIndex = [cell.accessibilityHint intValue];
        if ([arrayOfSection count]) {
            mModel = (MessageModel*)[arrayOfSection objectAtIndex:rowIndex];
        }
    }
    
    switch (index) {
        case 0:
        {
            if(mModel.isPinned) {
                mModel.isPinned = NO;
                [[CoreDataController sharedManager] UpdateMessageModel:mModel];
                [self updateData];
                
            }
            else {
                mModel.isPinned = YES;
                alertViewTag = 0;
                
                [[CoreDataController sharedManager] UpdateMessageModel:mModel];
                [self updateData];
                
                [self.navigationController.view makeToast:@"The conversation was successfully pinned"];
                
//                [_customAlert showCustomAlertInView:self.view withMessage:@"Success" andDescription:@"The conversation was successfully pinned"];
            }
            
            
            break;
        }
        case 1:
        {
            alertViewTag = 1;
            selectedMessageModel = mModel;

            [_customAlert showCustomAlertInView:self.view withMessage:@"Archive Conversation" andDescription:@"Are you sure you want to archive this message? Please note that the conversation will be un-archived if another user sends a message in this conversation"];
            
            break;
        }
        default:
            break;
    }
    
    
    [cell hideUtilityButtonsAnimated:YES];
}

- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell
{
    // allow just one cell's utility button to be open at once
    return YES;
}

- (BOOL)swipeableTableViewCell:(SWTableViewCell *)cell canSwipeToState:(SWCellState)state
{
    switch (state) {
        case 1:
            // set to NO to disable all left utility buttons appearing
            return YES;
            break;
        case 2:
            // set to NO to disable all right utility buttons appearing
            return YES;
            break;
        default:
            break;
    }
    
    return YES;
}

- (void) sortMessagesDictionary {
    messagesDictArray = [[NSMutableArray alloc] init];
    [self getPinnedMsgs];
    
    for(int i=0; i<headersArray.count; i++) {
        NSMutableDictionary *tempDict = [headersArray objectAtIndex:i];
        NSMutableArray *messagesArrayForSpan = [self getMesagesForSpan:tempDict andArray:messages];
        if(messagesArrayForSpan.count>0){
            
            NSMutableDictionary *dictForDate = [[NSMutableDictionary alloc] init];
            
            [dictForDate setObject:messagesArrayForSpan forKey:@"array"];
            [dictForDate setObject:[tempDict objectForKey:@"day"] forKey:@"day"];
            
            [messagesDictArray addObject:dictForDate];
        }
    }
    
}

- (void) sortMessagesDictionaryForSearchedItems {
    messagesDictArrayforSearch = [[NSMutableArray alloc] init];
    [self getPinnedMsgsForSearch];
    
    for(int i=0; i<headersArray.count; i++) {
        NSMutableDictionary *tempDict = [headersArray objectAtIndex:i];
        NSMutableArray *messagesArrayForSpan = [self getMesagesForSpan:tempDict andArray:searchResults];
        if(messagesArrayForSpan.count>0){
            
            NSMutableDictionary *dictForDate = [[NSMutableDictionary alloc] init];
            
            [dictForDate setObject:messagesArrayForSpan forKey:@"array"];
            [dictForDate setObject:[tempDict objectForKey:@"day"] forKey:@"day"];
            
            [messagesDictArrayforSearch addObject:dictForDate];
        }
    }
    
}

- (void) getPinnedMsgs {
    NSMutableArray *messagesArray = [[NSMutableArray alloc] init];
    for(int i=0; i<messages.count; i++){
        MessageModel *mModel = [messages objectAtIndex:i];
        if(mModel.isPinned && !mModel.isArchieved) {
            [messagesArray addObject:mModel];
        }
    }
    
    NSMutableDictionary *dictForDate = [[NSMutableDictionary alloc] init];
    
    [dictForDate setObject:messagesArray forKey:@"array"];
    [dictForDate setObject:@"Pinned and important" forKey:@"day"];
    
    if(messagesArray.count>0) {
        [messagesDictArray addObject:dictForDate];
    }
}
- (void) getPinnedMsgsForSearch {
    NSMutableArray *messagesArray = [[NSMutableArray alloc] init];
    for(int i=0; i<searchResults.count; i++){
        MessageModel *mModel = [searchResults objectAtIndex:i];
        if(mModel.isPinned && !mModel.isArchieved) {
            [messagesArray addObject:mModel];
        }
    }
    
    NSMutableDictionary *dictForDate = [[NSMutableDictionary alloc] init];
    
    [dictForDate setObject:messagesArray forKey:@"array"];
    [dictForDate setObject:@"Pinned and important" forKey:@"day"];
    
    if(messagesArray.count>0) {
        [messagesDictArrayforSearch addObject:dictForDate];
    }
}

- (NSMutableArray*) getMesagesForSpan : (NSMutableDictionary*)tempDict andArray :(NSMutableArray*)scopeArray {
    NSMutableArray *messagesArray = [[NSMutableArray alloc] init];
    
    NSString *day = [tempDict objectForKey:@"day"];
    NSDate *date = [tempDict objectForKey:@"date"];
    
    if ([day isEqualToString:@"Today"] || [day isEqualToString:@"Yesterday"] || [day isEqualToString:@"Monday"] || [day isEqualToString:@"Tuesday"] || [day isEqualToString:@"Wednesday"] || [day isEqualToString:@"Thursday"] || [day isEqualToString:@"Friday"] || [day isEqualToString:@"Saturday"] || [day isEqualToString:@"Sunday"]){
        for(int i=0; i<scopeArray.count; i++){
            MessageModel *mModel = [scopeArray objectAtIndex:i];
            if(!mModel.isPinned && !mModel.isArchieved) {
                
                NSString *dateString = mModel.timeStamp;
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                NSLocale *enUSPosixLocale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
                [formatter setLocale:enUSPosixLocale];
                [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ"];
                NSDate *dateForMessage = [formatter dateFromString:dateString];
                
                BOOL isSameDay = [[NSCalendar currentCalendar] isDate:dateForMessage inSameDayAsDate:date];
                if(isSameDay) {
                    [messagesArray addObject:mModel];
                }
            }
        }
    }
    else if([day isEqualToString:@"Last Week"]) {
        // date is between this week and last week
        NSDate *date1 = [[Utils getStartDateOfWeek] dateByAddingTimeInterval: -86400.0] ;
        NSDate *date2 = [Utils getStartOfLastWeek];
        
        for(int i=0; i<scopeArray.count; i++){
            MessageModel *mModel = [scopeArray objectAtIndex:i];
            if(!mModel.isPinned && !mModel.isArchieved) {
                NSString *dateString = mModel.timeStamp;
                
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                NSLocale *enUSPosixLocale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
                [formatter setLocale:enUSPosixLocale];
                [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ"];
                NSDate *dateForMessage = [formatter dateFromString:dateString];
                
                BOOL isSameDay = [self isDate:dateForMessage inRangeFirstDate:date2 lastDate:date1];
                if(isSameDay) {
                    [messagesArray addObject:mModel];
                }
            }
        }
    }
    
    else if([day isEqualToString:@"This Month"]) {
        // date is between this week and last week
        NSDate *date1 = [[Utils getStartOfLastWeek] dateByAddingTimeInterval: -86400.0] ;
        NSDate *date2 = [Utils getStartOfThisMonth];
        
        for(int i=0; i<scopeArray.count; i++){
            MessageModel *mModel = [scopeArray objectAtIndex:i];
            if(!mModel.isPinned && !mModel.isArchieved) {
                NSString *dateString = mModel.timeStamp;
                
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                NSLocale *enUSPosixLocale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
                [formatter setLocale:enUSPosixLocale];
                [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ"];
                NSDate *dateForMessage = [formatter dateFromString:dateString];
                
                BOOL isSameDay = [self isDate:dateForMessage inRangeFirstDate:date2 lastDate:date1];
                if(isSameDay) {
                    [messagesArray addObject:mModel];
                }
            }
            
        }
    }
    
    else if([day isEqualToString:@"Last Month"]) {
        // date is between this week and last week
        NSDate *date1 = [[Utils getStartOfThisMonth] dateByAddingTimeInterval: -86400.0] ;
        NSDate *date2 = [Utils getStartOfLastMonth];
        
        for(int i=0; i<scopeArray.count; i++){
            MessageModel *mModel = [scopeArray objectAtIndex:i];
            if(!mModel.isPinned && !mModel.isArchieved) {
                NSString *dateString = mModel.timeStamp;
                
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                NSLocale *enUSPosixLocale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
                [formatter setLocale:enUSPosixLocale];
                [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ"];
                NSDate *dateForMessage = [formatter dateFromString:dateString];
                
                BOOL isSameDay = [self isDate:dateForMessage inRangeFirstDate:date2 lastDate:date1];
                if(isSameDay) {
                    [messagesArray addObject:mModel];
                }
            }
        }
    }
    return messagesArray;
}



- (BOOL)isDate:(NSDate *)date inRangeFirstDate:(NSDate *)firstDate lastDate:(NSDate *)lastDate {
    return [date compare:firstDate] == NSOrderedDescending &&
    [date compare:lastDate]  == NSOrderedAscending;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
}

-(void)RapporrAlertOK{
    if(alertViewTag == 1) {
        if(selectedMessageModel.isArchieved) {
            selectedMessageModel.isArchieved = false;
        }
        else {
            selectedMessageModel.isArchieved = true;
        }
        [[CoreDataController sharedManager] UpdateMessageModel:selectedMessageModel];
        
        [self updateData];
    }
    
    [_customAlert removeCustomAlertFromViewInstantly];
}
-(void)RapporrAlertCancel{
    [_customAlert removeCustomAlertFromViewInstantly];
}



- (IBAction)btnChangeCompany:(id)sender {

    FDActionSheet *sheet = [[FDActionSheet alloc] initWithTitle:@"Change Company" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Join a Company", @"Create New Company", nil];
    [sheet show];
}

- (void)actionSheetCancel:(FDActionSheet *)actionSheet;{}
- (void)actionSheet:(FDActionSheet *)sheet clickedButtonIndex:(NSInteger)buttonIndex;{
    
    if(buttonIndex == 0){
        JoinCompanyVC *joinCompanyVC = [self.storyboard instantiateViewControllerWithIdentifier:@"JoinCompanyVC"];
        joinCompanyVC.isPushedFromHome = YES;
        [self.navigationController hidesBottomBarWhenPushed];
        [self.navigationController pushViewController:joinCompanyVC animated:YES];
        
    }else if(buttonIndex == 1){
        WelcomeRapporrVC *newRapporrVC = [self.storyboard instantiateViewControllerWithIdentifier:@"WelcomeRapporrVC"];
        [self.navigationController hidesBottomBarWhenPushed];
        [self.navigationController pushViewController:newRapporrVC  animated:YES];
    }
}



@end
