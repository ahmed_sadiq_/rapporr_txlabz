//
//  JoinCompanyVC.h
//  Rapporr
//
//  Created by Ahmed Sadiq on 18/03/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CompanyModel.h"

@interface JoinCompanyVC : UIViewController<UITableViewDelegate,UITableViewDataSource> {
    CompanyModel *selectedCompanyModel;
        AppDelegate *delegate;
        
    }
    
@property (nonatomic) Reachability *internetReachability;
@property (strong, nonatomic) NSMutableArray       *companyArray;
@property (strong, nonatomic) NSString             *userPhoneNumber;
@property (strong, nonatomic) IBOutlet UITableView *companiesTblView;
@property (strong, nonatomic) IBOutlet UIView      *tableViewContainer;
@property (nonatomic) BOOL isPushedFromHome;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;

- (IBAction)btnBack:(id)sender;

@end
