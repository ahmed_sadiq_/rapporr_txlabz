//
//  InfoHubVC.m
//  Rapporr
//
//  Created by txlabz on 17/04/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "InfoHubVC.h"

@interface InfoHubVC ()

@end

@implementation InfoHubVC

- (void)viewDidLoad {
    [super viewDidLoad];

    [self adjustTabBarImageOffset];
    self.webView.scrollView.backgroundColor = [UIColor colorWithCSS:@"#FF5721"];
    
    
    NSString *url = @"http://google.com";
    NSURL *nsUrl = [NSURL URLWithString:url];
    NSURLRequest *request = [NSURLRequest requestWithURL:nsUrl cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30];
    dispatch_queue_t workerQueue = dispatch_queue_create("QueueIdentifier", NULL);
    dispatch_async(workerQueue, ^ {
        [self.webView loadRequest:request];
    });
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) adjustTabBarImageOffset {
    UITabBar *tabBar = self.navigationController.tabBarController.tabBar;
    UITabBarItem *tabBarItem1 = [tabBar.items objectAtIndex:0];
    
    tabBarItem1.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
    self.title = nil;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
