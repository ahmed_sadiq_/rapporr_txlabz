//
//  NetworkManager.m
//  Rapporr
//
//  Created by Ahmed Sadiq on 20/03/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "Constants.h"
#import "DataManager.h"
#import "SVProgressHUD.h"
#import "NetworkManager.h"
#import "AFNetworking/AFNetworking.h"


@implementation NetworkManager

+ (NSError *)createErrorMessageForObject:(id)responseObject
{
    
    NSString * responseStr = @"";
    
    if (responseObject[@"message"])
        responseStr = [NSString stringWithFormat:@"%@",responseObject[@"message"]];
    else
        responseStr = [NSString stringWithFormat:@"API Response Error!\n%@", responseObject];
    
    
    NSError *error = [NSError errorWithDomain:@"Failed!"
                                         code:100
                                     userInfo:@{
                                                // NSLocalizedDescriptionKey:responseObject[@"message"]
                                                
                                                NSLocalizedDescriptionKey:responseStr
                                                
                                                }];
    NSLog(@"Failed with Response: %@", responseObject);
    return error;
}
+(void)  getCompaniesForMobileNumber:(NSString *) uri parameters:(NSDictionary *) params success:(loadSuccess) success failure:(loadFailure) failure {
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
    
    uri = [NSString stringWithFormat:@"%@%@",BASE_URL,uri];
    uri = [uri stringByAddingPercentEscapesUsingEncoding:
           NSUTF8StringEncoding];
    
    NSURL *baseURL = [[NSURL alloc] initWithString:BASE_URL];
    NSString *signature = [DataManager generateSignature:uri];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager.requestSerializer setValue:signature forHTTPHeaderField:@"signature"];
    [manager.requestSerializer setValue:COOKIE forHTTPHeaderField:@"Cookie"];
    [manager.requestSerializer setValue:RAPPORRVERSION forHTTPHeaderField:@"rapporrverison"];
    
    [manager POST:uri parameters:params success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [SVProgressHUD dismiss];
         
         success(responseObject);
         
         
     }
          failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [SVProgressHUD dismiss];
         NSLog(@"Failure: %@", error.localizedDescription);
         failure(error);
     }];
}

+(void)  generateVerificationCode:(NSString *) uri parameters:(NSDictionary *) params success:(loadSuccess) success failure:(loadFailure) failure {
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
    
    uri = [NSString stringWithFormat:@"%@%@",BASE_URL,uri];
    uri = [uri stringByAddingPercentEscapesUsingEncoding:
           NSUTF8StringEncoding];
    
    NSURL *baseURL = [[NSURL alloc] initWithString:BASE_URL];
    NSString *signature = [DataManager generateSignature:uri];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager.requestSerializer setValue:signature forHTTPHeaderField:@"signature"];
    [manager.requestSerializer setValue:COOKIE forHTTPHeaderField:@"Cookie"];
    [manager.requestSerializer setValue:RAPPORRVERSION forHTTPHeaderField:@"rapporrverison"];
    
    [manager POST:uri parameters:params success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [SVProgressHUD dismiss];
         
         success(responseObject);
         
         
     }
          failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [SVProgressHUD dismiss];
         NSLog(@"Failure: %@", error.localizedDescription);
         failure(error);
     }];
}

+(void)  validatePinCode:(NSString *) uri parameters:(NSDictionary *) params success:(loadSuccess) success failure:(loadFailure) failure {
    
    
    NSString *hostId = [params objectForKey:@"hostId"];
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
    
    uri = [NSString stringWithFormat:@"%@%@",BASE_URL,uri];
    uri = [uri stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSString *signatureStr = [NSString stringWithFormat:@"%@%@",uri,RAPPORR_API_KEY];
    
    
    NSURL *baseURL = [[NSURL alloc] initWithString:BASE_URL];
    NSString *signature = [DataManager generateSignature:signatureStr];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager.requestSerializer setValue:signature forHTTPHeaderField:@"signature"];
    [manager.requestSerializer setValue:COOKIE forHTTPHeaderField:@"Cookie"];
    [manager.requestSerializer setValue:RAPPORRVERSION forHTTPHeaderField:@"rapporrverison"];
    [manager.requestSerializer setValue:hostId forHTTPHeaderField:@"Rapporrhost"];
    
    [manager POST:uri parameters:params success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [SVProgressHUD dismiss];
         success(responseObject);
         
         
     }
          failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [SVProgressHUD dismiss];
         NSLog(@"Failure: %@", error.localizedDescription);
         failure(error);
     }];
}

+(void) createNewRapporr:(NSString *) uri parameters:(NSDictionary *) params success:(loadSuccess) success failure:(loadFailure) failure {
    
    
    NSString *hostId = [params objectForKey:@"hostId"];
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
    
    uri = [NSString stringWithFormat:@"%@%@",BASE_URL,uri];
    uri = [uri stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSString *signatureStr = [NSString stringWithFormat:@"%@%@",uri,RAPPORR_API_KEY];
    
    NSURL *baseURL = [[NSURL alloc] initWithString:BASE_URL];
    NSString *signature = [DataManager generateSignature:signatureStr];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager.requestSerializer setValue:signature forHTTPHeaderField:@"signature"];
    [manager.requestSerializer setValue:COOKIE forHTTPHeaderField:@"Cookie"];
    [manager.requestSerializer setValue:RAPPORRVERSION forHTTPHeaderField:@"rapporrverison"];
    [manager.requestSerializer setValue:hostId forHTTPHeaderField:@"Rapporrhost"];
    
    [manager POST:uri parameters:params success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [SVProgressHUD dismiss];
         
         success(responseObject);
         
     }
          failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [SVProgressHUD dismiss];
         NSLog(@"Failure: %@", error.localizedDescription);
         failure(error);
     }];
}



+(void) fetchConversationAPI:(NSString *) uri parameters:(NSDictionary *) params success:(loadSuccess) success failure:(loadFailure) failure {
    
    
    NSString *hostId = [params objectForKey:@"hostId"];
    NSString *userId = [params objectForKey:@"userID"];
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
    
    NSString *authorization = [DataManager createAuthHeader:userId andHostId:hostId];
    
    uri = [NSString stringWithFormat:@"%@%@",BASE_URL,uri];
    uri = [uri stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSString *signatureStr = [NSString stringWithFormat:@"%@%@",uri,RAPPORR_API_KEY];
    
    NSURL *baseURL = [[NSURL alloc] initWithString:BASE_URL];
    NSString *signature = [DataManager generateSignature:signatureStr];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager.requestSerializer setValue:signature forHTTPHeaderField:@"signature"];
    [manager.requestSerializer setValue:COOKIE forHTTPHeaderField:@"Cookie"];
    [manager.requestSerializer setValue:RAPPORRVERSION forHTTPHeaderField:@"rapporrverison"];
    [manager.requestSerializer setValue:hostId forHTTPHeaderField:@"Rapporrhost"];
    [manager.requestSerializer setValue:authorization forHTTPHeaderField:@"Authorization"];
    
    NSString *pathToCert = [[NSBundle mainBundle]pathForResource:@"imagesrapporrappcom" ofType:@"crt"];
    NSData *localCertificate = [NSData dataWithContentsOfFile:pathToCert];
    
    manager.securityPolicy.pinnedCertificates = @[localCertificate];
    
    [manager GET:uri parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [SVProgressHUD dismiss];
         
         success(responseObject);
     }
         failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [SVProgressHUD dismiss];
         NSLog(@"Failure: %@", error.localizedDescription);
         failure(error);
     }];
}

+(void)  validateMobileNumber:(NSString *) uri parameters:(NSString *) number success:(loadSuccess) success failure:(loadFailure) failure {
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
    
    uri = [NSString stringWithFormat:@"%@%@%@",BASE_URL,URI_VALIDATE_NUMBER,number];
    uri = [uri stringByAddingPercentEscapesUsingEncoding:
           NSUTF8StringEncoding];
    
    NSURL *baseURL = [[NSURL alloc] initWithString:BASE_URL];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager GET:uri parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [SVProgressHUD dismiss];
         
         NSError* error;
         
         success(responseObject);
     }
         failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [SVProgressHUD dismiss];
         NSLog(@"Failure: %@", error.localizedDescription);
         failure(error);
     }];
}
@end
