//
//  ConversationDetailVC.h
//  Rapporr
//
//  Created by txlabz on 17/04/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VCFloatingActionButton.h"

@interface ConversationDetailVC : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *editMessageContainer;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldEditMessage;


- (IBAction)btnBack:(id)sender;
- (IBAction)btnSendMessage:(id)sender;
- (IBAction)btnAddFile:(id)sender;


@property (strong, nonatomic) VCFloatingActionButton *addMenuButton;



@end
