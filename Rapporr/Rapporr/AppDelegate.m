//
//  AppDelegate.m
//  Rapporr
//
//  Created by Ahmed Sadiq on 15/03/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "Constants.h"
#import "AppDelegate.h"
#import <Fabric/Fabric.h>
#import "RapporrManager.h"
#import "VerifiedCompanyModel.h"
#import <Crashlytics/Crashlytics.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {


    self.window.backgroundColor = [UIColor whiteColor];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    
    
    self.internetReachability = [Reachability reachabilityForInternetConnection];
    [self updateInterfaceWithReachability:self.internetReachability];

    Reachability* reach = [Reachability reachabilityWithHostName:@"www.google.com"];
    
    reach.reachableBlock = ^(Reachability*reach)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"REACHABLE!");
        });
    };
    
    reach.unreachableBlock = ^(Reachability*reach)
    {
        NSLog(@"UNREACHABLE!");
    };
    
    [reach startNotifier];
    
    [self initiateThirdParties];
    [self addColoredStatusPlayer];
    
    if([[CoreDataController sharedManager] ifAnyVerifiedCompant]) {
    
        NSMutableArray *fetchedCompanies = [[CoreDataController sharedManager] getVerifiedCompanies];
        VerifiedCompanyModel *tempVcModel = [[VerifiedCompanyModel alloc] initWithManagedObject:[fetchedCompanies objectAtIndex:0]];
        [RapporrManager sharedManager].vcModel = tempVcModel;
        //Change Root View Controller
        
        NSDictionary *userInfo = [NSDictionary dictionaryWithObject:tempVcModel forKey:@"CompanyModel"];
        dispatch_async(dispatch_get_main_queue(),^{

        [[NSNotificationCenter defaultCenter] postNotificationName:@"VerifiedCompanyModel" object:userInfo];
        });
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        UITabBarController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"mainTabBarController"];
        
        self.window.rootViewController = rootViewController;
        [self.window makeKeyAndVisible];
    }
    
    return YES;
}


/*!
 * Called by Reachability whenever status changes.
 */
- (void) reachabilityChanged:(NSNotification *)note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass:[Reachability class]]);
    [self updateInterfaceWithReachability:curReach];
}

- (void)updateInterfaceWithReachability:(Reachability *)reachability
{
    if (reachability == self.internetReachability)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            NetworkStatus netStatus = [reachability currentReachabilityStatus];
            BOOL connectionRequired = [reachability connectionRequired];
            NSString* statusString = @"";
            
            
            if (netStatus == NotReachable)
            {
                statusString = NSLocalizedString(@"No Internet Connection", @"access is not available");
                
                [RKDropdownAlert dismissAllAlert];
                [RKDropdownAlert title:@"" message:@"No Internet Connection" backgroundColor:[UIColor colorWithCSS:@"#FF5721"] textColor:[UIColor whiteColor] time:5 delegate:nil autoHide:NO];
                connectionRequired = NO;
                
            }else if (netStatus == ReachableViaWWAN || netStatus == ReachableViaWiFi){
                [RKDropdownAlert dismissAllAlert];

                if (!self.appJustStartedStatus) {
                    self.appJustStartedStatus = YES;
                }else{
                    //                [self updateData];
                    statusString = NSLocalizedString(@"Connected", @"");
                    [RKDropdownAlert title:@"" message:@"Connected" backgroundColor:[UIColor colorWithCSS:@"#00B36D"] textColor:[UIColor whiteColor] time:5 delegate:nil autoHide:YES];

                }
                connectionRequired = YES;
            }
        });
    }
}



- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Third Party Invocation 

- (void) initiateThirdParties {
    [Fabric with:@[[Crashlytics class]]];
}

#pragma mark - UINavigation Methods 

- (void) addColoredStatusPlayer {
    
    UIView* statusBg = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.window.frame.size.width, 20)];
    statusBg.backgroundColor = statusBarColor;
    
    //Add the view behind the status bar
    [self.window.rootViewController.view addSubview:statusBg];
}


- (NSManagedObjectContext *) managedObjectContext {
    if (managedObjectContext != nil) {
        return managedObjectContext;
    }
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        managedObjectContext = [[NSManagedObjectContext alloc] init];
        [managedObjectContext setPersistentStoreCoordinator: coordinator];
    }
    
    return managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel {
    if (managedObjectModel != nil) {
        return managedObjectModel;
    }
    managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil] ;
    
    return managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator

{
    
    if (persistentStoreCoordinator != nil) {
        return persistentStoreCoordinator;
    }
    NSURL *storeUrl = [NSURL fileURLWithPath: [[self  applicationDocumentsDirectory]
                                               stringByAppendingPathComponent: @"Rapporr.sqlite"]];
    NSError *error = nil;
    persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc]
                                  initWithManagedObjectModel:[self managedObjectModel]];
    if(![persistentStoreCoordinator      addPersistentStoreWithType:NSSQLiteStoreType configuration:nil   URL:storeUrl options:nil error:&error])
    {
        /*Error for store creation should be handled in here*/
    }
    
    return persistentStoreCoordinator;
}
- (NSString *)applicationDocumentsDirectory {
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

@end
