//
//  CreateConversationVC.m
//  Rapporr
//
//  Created by txlabz on 14/04/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "CreateConversationVC.h"


@interface CreateConversationVC ()
@property (nonatomic, strong) NSArray *dataArray;
@property (nonatomic, strong) NSMutableDictionary *contentOffsetDictionary;
@end

@implementation CreateConversationVC

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)btnCancel:(id)sender {

    [self.navigationController popViewControllerAnimated:NO];
}


@end
