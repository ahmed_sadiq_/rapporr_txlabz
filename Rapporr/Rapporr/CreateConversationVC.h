//
//  CreateConversationVC.h
//  Rapporr
//
//  Created by txlabz on 14/04/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+Hex.h"
#import "Reachability.h"
#import "RKDropdownAlert.h"

@interface CreateConversationVC : UIViewController<UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
- (IBAction)btnCancel:(id)sender;
@end
