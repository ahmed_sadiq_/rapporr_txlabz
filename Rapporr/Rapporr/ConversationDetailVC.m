//
//  ConversationDetailVC.m
//  Rapporr
//
//  Created by txlabz on 17/04/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "ConversationDetailVC.h"
#import "ConversationMessageCell.h"

@interface ConversationDetailVC ()

@end

@implementation ConversationDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];


    [self addFloatingButton];

}

- (void)addFloatingButton {
    CGRect floatFrame = CGRectMake([UIScreen mainScreen].bounds.size.width - 44 - 20, [UIScreen mainScreen].bounds.size.height - 44 - 60, 44, 44);
    
    self.addMenuButton = [[VCFloatingActionButton alloc]initWithFrame:floatFrame normalImage:[UIImage imageNamed:@"add_plus_new"] andPressedImage:[UIImage imageNamed:@"add_plus_new_press"] withScrollview:self.tableView];
    
    self.addMenuButton.hideWhileScrolling = YES;
    self.addMenuButton.delegate = self;
    
    [self.view addSubview:self.addMenuButton];
}

-(void) didMenuButtonTapped;
{
    [self.txtFieldEditMessage becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;{

    ConversationMessageCell * cell = (ConversationMessageCell *)[tableView dequeueReusableCellWithIdentifier:@"ConvMessageCell"];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"ConversationMessageCell" owner:self options:nil];
        cell=[nib objectAtIndex:0];
    }
    
    cell.imgView.layer.cornerRadius = 5;
    cell.imgView.clipsToBounds = YES;

    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 84;
}

-(void)keyboardWillShow:(NSNotification *)notification {
    
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    float height = MIN(keyboardSize.height,keyboardSize.width);
    CGRect rect = self.editMessageContainer.frame;
    CGRect tableViewRect = self.tableView.frame;

    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    rect.origin.y = self.view.frame.size.height - height - rect.size.height;
    tableViewRect.size.height = self.view.frame.size.height - height - rect.size.height - 2;
   
    self.tableView.frame = tableViewRect;

    self.editMessageContainer.frame = rect;
    [UIView commitAnimations];

    NSLog(@"%f",self.view.frame.size.height - height - rect.size.height);
}

-(void)keyboardWillHide:(NSNotification *)notification {
   
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;

    float height = MIN(keyboardSize.height,keyboardSize.width);
    CGRect rect = self.editMessageContainer.frame;
    CGRect tableViewRect = self.tableView.frame;

    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    rect.origin.y = self.view.frame.size.height - rect.size.height;
    tableViewRect.size.height = self.view.frame.size.height - rect.size.height - 2;
    self.tableView.frame = tableViewRect;

    self.editMessageContainer.frame = rect;
    [UIView commitAnimations];
    
    NSLog(@"%f",self.view.frame.size.height - height - rect.size.height);
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{

    [self.view endEditing:YES];
    return YES;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnSendMessage:(id)sender {
    [self.view endEditing:YES];
}

- (IBAction)btnAddFile:(id)sender {
    
}


@end
