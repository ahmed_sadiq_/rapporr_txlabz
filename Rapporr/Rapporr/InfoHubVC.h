//
//  InfoHubVC.h
//  Rapporr
//
//  Created by txlabz on 17/04/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfoHubVC : UIViewController

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@end
