//
//  RapporrAlertView.h
//  Rapporr
//
//  Created by Ahmed Sadiq on 15/04/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RapporrAlertViewDelegate
-(void)RapporrAlertOK;
-(void)RapporrAlertCancel;
@end

@interface RapporrAlertView : UIViewController
@property (nonatomic, retain) id<RapporrAlertViewDelegate> delegate;
@property (strong, nonatomic) IBOutlet UILabel *titleLbl;
@property (strong, nonatomic) IBOutlet UILabel *descLbl;
@property (strong, nonatomic) IBOutlet UIButton *btnCANCEL;
@property (strong, nonatomic) IBOutlet UIButton *btnOK;

@property (weak, nonatomic) IBOutlet UIView *viewMessage;

- (IBAction)okPressed:(id)sender;
- (IBAction)cancelPressed:(id)sender;

-(void)showCustomAlertInView:(UIView *)targetView withMessage:(NSString *)message andDescription:(NSString*)desc;
-(void)removeCustomAlertFromView;
-(void)removeCustomAlertFromViewInstantly;
-(void)removeOkayButton:(BOOL)shouldRemove;
-(void)removeCancelButton:(BOOL)shouldRemove;
-(BOOL)isOkayButtonRemoved;
-(BOOL)isCancelButtonRemoved;

@end


