//
//  ConversationMessageCell.h
//  Rapporr
//
//  Created by txlabz on 17/04/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConversationMessageCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblMeggage;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblSeenBy;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;

@end
