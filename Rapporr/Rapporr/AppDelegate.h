//
//  AppDelegate.h
//  Rapporr
//
//  Created by Ahmed Sadiq on 15/03/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "CoreDataController.h"
#import "Reachability.h"
#import "RKDropdownAlert.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate,NSObject> {
    NSManagedObjectModel *managedObjectModel;
    NSManagedObjectContext *managedObjectContext;
    NSPersistentStoreCoordinator *persistentStoreCoordinator;
}

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;

// Reachability for network connectivity check
@property (nonatomic) Reachability *internetReachability;
@property (nonatomic) BOOL appJustStartedStatus;

- (NSString *)applicationDocumentsDirectory;
- (void)updateInterfaceWithReachability:(Reachability *)reachability;

@end

