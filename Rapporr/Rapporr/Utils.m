//
//  Utils.m
//  Rapporr
//
//  Created by Ahmed Sadiq on 18/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "Utils.h"
#import "NSDate+NKLocalizedWeekday.h"
#import "RKDropdownAlert.h"

@implementation Utils

+(UIAlertController *) showCustomAlert:(NSString *) title andMessage : (NSString *) message {
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:title
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Okay"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                }];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"Cancel"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle no, thanks button
                               }];
    
    [alert addAction:yesButton];
    //[alert addAction:noButton];
    
    return alert;
    
}

+ (BOOL)validateEmail:(NSString *)candidate {
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{1,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}

+ (NSString*)getTelephonicCountryCode:(NSString *)countryCode {
    NSDictionary *countryCodeDict = [self getCountryCodeDictionary];
    NSString *telephonicCode = [countryCodeDict objectForKey:countryCode];
    telephonicCode = [NSString stringWithFormat:@"+%@",telephonicCode];
    
    return telephonicCode;
    
}

+ (NSDictionary *)getCountryCodeDictionary {
    return [NSDictionary dictionaryWithObjectsAndKeys:@"972", @"IL",
            @"93", @"AF", @"355", @"AL", @"213", @"DZ", @"1", @"AS",
            @"376", @"AD", @"244", @"AO", @"1", @"AI", @"1", @"AG",
            @"54", @"AR", @"374", @"AM", @"297", @"AW", @"61", @"AU",
            @"43", @"AT", @"994", @"AZ", @"1", @"BS", @"973", @"BH",
            @"880", @"BD", @"1", @"BB", @"375", @"BY", @"32", @"BE",
            @"501", @"BZ", @"229", @"BJ", @"1", @"BM", @"975", @"BT",
            @"387", @"BA", @"267", @"BW", @"55", @"BR", @"246", @"IO",
            @"359", @"BG", @"226", @"BF", @"257", @"BI", @"855", @"KH",
            @"237", @"CM", @"1", @"CA", @"238", @"CV", @"345", @"KY",
            @"236", @"CF", @"235", @"TD", @"56", @"CL", @"86", @"CN",
            @"61", @"CX", @"57", @"CO", @"269", @"KM", @"242", @"CG",
            @"682", @"CK", @"506", @"CR", @"385", @"HR", @"53", @"CU",
            @"537", @"CY", @"420", @"CZ", @"45", @"DK", @"253", @"DJ",
            @"1", @"DM", @"1", @"DO", @"593", @"EC", @"20", @"EG",
            @"503", @"SV", @"240", @"GQ", @"291", @"ER", @"372", @"EE",
            @"251", @"ET", @"298", @"FO", @"679", @"FJ", @"358", @"FI",
            @"33", @"FR", @"594", @"GF", @"689", @"PF", @"241", @"GA",
            @"220", @"GM", @"995", @"GE", @"49", @"DE", @"233", @"GH",
            @"350", @"GI", @"30", @"GR", @"299", @"GL", @"1", @"GD",
            @"590", @"GP", @"1", @"GU", @"502", @"GT", @"224", @"GN",
            @"245", @"GW", @"595", @"GY", @"509", @"HT", @"504", @"HN",
            @"36", @"HU", @"354", @"IS", @"91", @"IN", @"62", @"ID",
            @"964", @"IQ", @"353", @"IE", @"972", @"IL", @"39", @"IT",
            @"1", @"JM", @"81", @"JP", @"962", @"JO", @"77", @"KZ",
            @"254", @"KE", @"686", @"KI", @"965", @"KW", @"996", @"KG",
            @"371", @"LV", @"961", @"LB", @"266", @"LS", @"231", @"LR",
            @"423", @"LI", @"370", @"LT", @"352", @"LU", @"261", @"MG",
            @"265", @"MW", @"60", @"MY", @"960", @"MV", @"223", @"ML",
            @"356", @"MT", @"692", @"MH", @"596", @"MQ", @"222", @"MR",
            @"230", @"MU", @"262", @"YT", @"52", @"MX", @"377", @"MC",
            @"976", @"MN", @"382", @"ME", @"1", @"MS", @"212", @"MA",
            @"95", @"MM", @"264", @"NA", @"674", @"NR", @"977", @"NP",
            @"31", @"NL", @"599", @"AN", @"687", @"NC", @"64", @"NZ",
            @"505", @"NI", @"227", @"NE", @"234", @"NG", @"683", @"NU",
            @"672", @"NF", @"1", @"MP", @"47", @"NO", @"968", @"OM",
            @"92", @"PK", @"680", @"PW", @"507", @"PA", @"675", @"PG",
            @"595", @"PY", @"51", @"PE", @"63", @"PH", @"48", @"PL",
            @"351", @"PT", @"1", @"PR", @"974", @"QA", @"40", @"RO",
            @"250", @"RW", @"685", @"WS", @"378", @"SM", @"966", @"SA",
            @"221", @"SN", @"381", @"RS", @"248", @"SC", @"232", @"SL",
            @"65", @"SG", @"421", @"SK", @"386", @"SI", @"677", @"SB",
            @"27", @"ZA", @"500", @"GS", @"34", @"ES", @"94", @"LK",
            @"249", @"SD", @"597", @"SR", @"268", @"SZ", @"46", @"SE",
            @"41", @"CH", @"992", @"TJ", @"66", @"TH", @"228", @"TG",
            @"690", @"TK", @"676", @"TO", @"1", @"TT", @"216", @"TN",
            @"90", @"TR", @"993", @"TM", @"1", @"TC", @"688", @"TV",
            @"256", @"UG", @"380", @"UA", @"971", @"AE", @"44", @"GB",
            @"1", @"US", @"598", @"UY", @"998", @"UZ", @"678", @"VU",
            @"681", @"WF", @"967", @"YE", @"260", @"ZM", @"263", @"ZW",
            @"591", @"BO", @"673", @"BN", @"61", @"CC", @"243", @"CD",
            @"225", @"CI", @"500", @"FK", @"44", @"GG", @"379", @"VA",
            @"852", @"HK", @"98", @"IR", @"44", @"IM", @"44", @"JE",
            @"850", @"KP", @"82", @"KR", @"856", @"LA", @"218", @"LY",
            @"853", @"MO", @"389", @"MK", @"691", @"FM", @"373", @"MD",
            @"258", @"MZ", @"970", @"PS", @"872", @"PN", @"262", @"RE",
            @"7", @"RU", @"590", @"BL", @"290", @"SH", @"1", @"KN",
            @"1", @"LC", @"590", @"MF", @"508", @"PM", @"1", @"VC",
            @"239", @"ST", @"252", @"SO", @"47", @"SJ", @"963", @"SY",
            @"886", @"TW", @"255", @"TZ", @"670", @"TL", @"58", @"VE",
            @"84", @"VN", @"1", @"VG", @"1", @"VI", nil];
}

#pragma mark - Server Communication Helpher Method
+ (NSData*)encodeDictionary:(NSDictionary*)dictionary {
    NSMutableArray *parts = [[NSMutableArray alloc] init];
    for (NSString *key in dictionary) {
        NSString *encodedValue = [[dictionary objectForKey:key] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *encodedKey = [key stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *part = [NSString stringWithFormat: @"%@=%@", encodedKey, encodedValue];
        [parts addObject:part];
    }
    NSString *encodedDictionary = [parts componentsJoinedByString:@"&"];
    return [encodedDictionary dataUsingEncoding:NSUTF8StringEncoding];
}



+ (NSString *) dateDifference:(NSDate *)date
{
    const NSTimeInterval secondsPerDay = 60 * 60 * 24;
    NSTimeInterval diff = [date timeIntervalSinceNow] * -1.0;
    
//    if (diff < 0)
//        return @"In the future";
//    
    diff /= secondsPerDay; // get the number of days
    
    if (diff < 1)
        return @"Today";
    else if (diff < 2)
        return @"Yesterday";
    else if (diff < 8)
        return @"Last week";
    else
        return [date description]; // use a date formatter if necessary
}

+ (NSMutableArray*) getTableViewSectionByDates {
    
    
    
    NSMutableArray *headerDates = [[NSMutableArray alloc] init];
    
    // Right now, you can remove the seconds into the day if you want
    NSDate *today = [NSDate date];
    NSDate *yesterday = [today dateByAddingTimeInterval: -86400.0];
    NSDate *thisWeek  = [self getStartDateOfWeek];
    NSDate *lastWeek  = [self getStartOfLastWeek];
    
    // To get the correct number of seconds in each month use NSCalendar
    NSDate *thisMonth = [self getStartOfThisMonth];
    NSDate *lastMonth = [self getStartOfLastMonth];
    
    
    NSMutableDictionary *headersDictionaryToday = [[NSMutableDictionary alloc] init];
    [headersDictionaryToday setObject:today forKey:@"date"];
    [headersDictionaryToday setObject:@"Today" forKey:@"day"];
    [headerDates addObject: headersDictionaryToday];
    
    NSMutableDictionary *headersDictionaryYesterday = [[NSMutableDictionary alloc] init];
    [headersDictionaryYesterday setObject:yesterday forKey:@"date"];
    [headersDictionaryYesterday setObject:@"Yesterday" forKey:@"day"];
    [headerDates addObject: headersDictionaryYesterday];
    
    
    NSMutableArray *daysOfThisWeek = [self getDatesBetweenTwoDates:thisWeek andEndDate:[yesterday dateByAddingTimeInterval: -86400.0]];
    
    daysOfThisWeek=[[[daysOfThisWeek reverseObjectEnumerator] allObjects] mutableCopy];
    
    for(int i=0; i<daysOfThisWeek.count; i++) {
        NSDate *tempDate = [daysOfThisWeek objectAtIndex:i];
        
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        [tempDictionary setObject:tempDate forKey:@"date"];
        [tempDictionary setObject:[self getWeekDayForDate:tempDate] forKey:@"day"];
        [headerDates addObject: tempDictionary];
    }
    
    NSMutableDictionary *headersDictionaryLastWeek = [[NSMutableDictionary alloc] init];
    [headersDictionaryLastWeek setObject:lastWeek forKey:@"date"];
    [headersDictionaryLastWeek setObject:@"Last Week" forKey:@"day"];
    [headerDates addObject: headersDictionaryLastWeek];
    
    NSMutableDictionary *headersDictionaryThisMonth = [[NSMutableDictionary alloc] init];
    [headersDictionaryThisMonth setObject:thisMonth forKey:@"date"];
    [headersDictionaryThisMonth setObject:@"This Month" forKey:@"day"];
    [headerDates addObject: headersDictionaryThisMonth];
    
    NSMutableDictionary *headersDictionaryLastMonth = [[NSMutableDictionary alloc] init];
    [headersDictionaryLastMonth setObject:lastMonth forKey:@"date"];
    [headersDictionaryLastMonth setObject:@"Last Month" forKey:@"day"];
    [headerDates addObject: headersDictionaryLastMonth];
    
    return headerDates;
}

+ (NSDate*) getStartDateOfWeek {
    NSDate *today = [NSDate date];
    NSCalendar *gregorian = [NSCalendar currentCalendar];
    
    // Get the weekday component of the current date
    NSDateComponents *weekdayComponents = [gregorian components:NSWeekdayCalendarUnit fromDate:today];
    
    NSDateComponents *componentsToSubtract = [[NSDateComponents alloc] init];
    
    [componentsToSubtract setDay: - ([weekdayComponents weekday] - [gregorian firstWeekday])];
    NSDate *beginningOfWeek = [gregorian dateByAddingComponents:componentsToSubtract toDate:today options:0];
    
    NSDateComponents *components = [gregorian components: (NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit)
                                                fromDate: beginningOfWeek];
    beginningOfWeek = [gregorian dateFromComponents: components];
    
    return beginningOfWeek;
}

+ (NSDate* ) getStartOfThisMonth {
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    components.day = 1;
    NSDate *firstDayOfMonthDate = [[NSCalendar currentCalendar] dateFromComponents: components];
    
    return firstDayOfMonthDate;

}

+ (NSDate* ) getStartOfLastWeek {
    return [[self getStartDateOfWeek] dateByAddingTimeInterval: -7*24*60*60];
    
}

+ (NSDate* ) getStartOfLastMonth {
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    components.day = 1;
    components.month = components.month-1;
    
    if(components.month < 1){
        components.month = 12;
        components.year = components.year-1;
    }
    
    NSDate *firstDayOfMonthDate = [[NSCalendar currentCalendar] dateFromComponents: components];
    NSLog(@"First day of month: %@", [firstDayOfMonthDate descriptionWithLocale:[NSLocale currentLocale]]);
    
    return firstDayOfMonthDate;
    
}

+ (NSString*) getWeekDayForDate : (NSDate*) date {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSWeekdayCalendarUnit fromDate:date];
    int weekdayNumber = [components weekday];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSString *weekdayString = [[formatter weekdaySymbols] objectAtIndex:weekdayNumber - 1];
    
    return weekdayString;
}

+ (NSMutableArray*) getDatesBetweenTwoDates :(NSDate*) startDate andEndDate :(NSDate*) endDate {
    
    NSMutableArray *dates = [@[startDate] mutableCopy];
    
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorianCalendar components:NSDayCalendarUnit fromDate:startDate toDate:endDate options:0];
    
    for (int i = 1; i < components.day; ++i) {
        NSDateComponents *newComponents = [NSDateComponents new];
        newComponents.day = i;
        
        NSDate *date = [gregorianCalendar dateByAddingComponents:newComponents
                                                          toDate:startDate
                                                         options:0];
        [dates addObject:date];
    }
    
    [dates addObject:endDate];
    
    return dates;
}




@end
